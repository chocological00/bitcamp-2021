
const URL_FORMAT = /(?:https?:\/\/)?(?:www\.)?(?:m.)?(?:youtube(?:-nocookie)?\.com\/(?:.*?(?:vi?|e|embed)(?:\/|=|%3D))|youtu\.be\/)([-\w+]+)/m;

export function validateURL(url: string): string|null
{
    let m;
	if ((m = URL_FORMAT.exec(url)) !== null) 
	{
        return m[1];
    } 
    else 
    {
        return null;
    }
}