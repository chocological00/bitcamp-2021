export interface RequestBody {
	url: string
}

export interface RequestResponse {
	uuid: string,
	video_id: string
}

export interface WebsocketQuery {
	uuid: string
}

export type FailMessage = 'bad_uuid' | 'processing' | 'download_error' | 'no_subtitles' | 'subtitle_error' | 'scene_error' | 'frame_error' | string

export type ProgressStage = 'download' | 'frames' | 'scenes';

export interface WebsocketProgress {
	stage: ProgressStage,
	progress: number
}

export interface WebSocketDone {
	video_id: string,
	s3_base_url: string,    // GET URL with baseurl/videoid_framenum.jpg
	num_frames: number
	captions: {
		start: number,		// IN SECONDS
		end: number			// IN SECONDS
		text: string
	}[],
	scenes: number[]		// IN SECONDS (rounded UP)
}