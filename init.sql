CREATE TABLE IF NOT EXISTS videos (
    vid_id text,
    subtitles json,
    frames_generated boolean,
    frames int,
    scenes json
)