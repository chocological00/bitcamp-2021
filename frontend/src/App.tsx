import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Landing, Loading, Edit, Show } from './pages';

class App extends React.Component<unknown, unknown> {
  render() {
    return(
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Landing} />
          <Route path='/loading' component={Loading} />
          <Route path='/edit' component={Edit} />
          <Route path='/show/:id' component={Show} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;