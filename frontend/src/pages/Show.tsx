import React from 'react';
import ReactMarkdown from 'react-markdown';

interface IState {
	sectionDict: { [key: number]: [Array<number>, string] };
	markdown: string;
}

class Show extends React.Component<unknown, IState> {
	constructor(props: unknown) {
		super(props);
		const d: { [key: number]: [Array<number>, string] } = JSON.parse(window.sessionStorage.getItem('finished') as string);
		let md = ''

		for (const sceneStart in d) {
			for (const n of d[sceneStart][0])
				md += `![image.jpg](${window.sessionStorage.getItem('s3-base')}/${window.sessionStorage.getItem('video-id')}_${Math.floor(n)}.jpg)\n`;
			md += `${d[sceneStart][1]}\n---\n`;
		}

		this.state = {
			sectionDict: d,
			markdown: md
		}
	}

	// https://stackoverflow.com/questions/2897619/using-html5-javascript-to-generate-and-save-a-file
	download(filename: string, text: string) {
		return () => {
			const pom = document.createElement('a');
			pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
			pom.setAttribute('download', filename);

			if (document.createEvent) {
					const event = document.createEvent('MouseEvents');
					event.initEvent('click', true, true);
					pom.dispatchEvent(event);
			}
			else {
					pom.click();
			}
		};
	}

	render() {
		return (
			<div className='grid-view-container'>
				<div className='flex-top-navigation-container'>
					<button className='btn btn-icon-only btn-primary btn-pill spaced-button' onClick={() => {window.location.href = '/'}} type='button' aria-label='Go to main page' data-toggle='tooltip' data-placement='bottom' title='Start Over'>
						<span aria-hidden='true' className='fas fa-chevron-left fa-lg'></span>
					</button>
					<button className='btn btn-icon-only btn-primary btn-pill spaced-button-right' onClick={this.download('blogified.md', this.state.markdown)} type='button' aria-label='Download as markdown' data-toggle='tooltip' data-placement='bottom' title='Download as .md'>
						<span aria-hidden='true' className='fas fa-file-download fa-lg'></span>
					</button>
				</div>
				<div className='text-viewer'>
					<ReactMarkdown>
						{this.state.markdown}
					</ReactMarkdown>
				</div>
			</div>
		)
	}

	componentDidMount() {
		console.log(this.props);
		($('[data-toggle="tooltip"]') as any).tooltip();
	}

	componentDidUpdate() {
		($('[data-toggle="tooltip"]') as any).tooltip();
	}
}

export default Show;