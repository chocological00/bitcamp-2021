import React from 'react';
import io from 'socket.io-client';
import { WebsocketProgress, FailMessage, WebSocketDone } from '../common/types';

interface IState {
	download: number;
	sample: number;
	scene: number;
	vid: string;
}

class Loading extends React.Component<unknown, IState> {
	constructor(props: unknown) {
		super(props);
		this.state = {
			download: 0,
			sample: 0,
			scene: 0,
			vid: ''
		}
	}

	componentDidMount() {
		const loc = '' + window.location.protocol + '//' + window.location.host;

		const id = window.sessionStorage.getItem('websocket-id');
		if (!id) window.location.href = '/';
		this.setState({ vid: window.sessionStorage.getItem('video-id') as string });
		const socket = io(loc, { path: '/ws/', query: { uuid: id as string } }); // TODO:
		console.log(socket);
		socket.on('progress', (data: WebsocketProgress) => {
			switch(data.stage) {
				case 'download':
					this.setState({ download: data.progress });
					break;
				case 'frames':
					this.setState({ sample: data.progress });
					break;
				case 'scenes':
					this.setState({ scene: data.progress });
					break;
			}
		});
		socket.on('fail', (data: FailMessage) => {
			switch(data) {
				case 'bad_uuid':
					alert('Connection identifier is corrupted. Please try again.');
					break;
				case 'processing':
					alert('Unexpected error has occured. Please try again at a later time.');
					break;
				case 'download_error':
					alert('An error has occured while downloading video. Please try again later or use a different video.');
					break;
				case 'no_subtitles':
					alert('This video does not have valid subtitles. Please try with a different video.');
					break;
				case 'subtitle_error':
					alert('The subtitle for this video does not have a standard format. Please try with a different video.');
					break;
				case 'scene_error':
					alert('YouTube has disabled the download option for this video. Please try with a different video.'); // AV1 codec error
					break;
				case 'frame_error':
					alert('The server is currently not able to process the video. Please try again at a later time.');
					break;
				default:
					alert('An unexpected error has occured!');
					break;
			}
		});
		socket.on('disconnect', () => {
			alert('Connection to the server was interrupted! If your internet is fine, this might mean that our server is being overloaded. Please try again later.');
		});
		socket.on('done', (data: WebSocketDone) => {
			window.sessionStorage.setItem('s3-base', data.s3_base_url);
			window.sessionStorage.setItem('frame-count', `${data.num_frames}`);
			window.sessionStorage.setItem('captions', JSON.stringify(data.captions));
			window.sessionStorage.setItem('scenes', JSON.stringify(data.scenes));
			window.location.href = '/edit';
		});
	}

	render() {
		return (
			<div className='fullscreen-container flex-center-container'>
				<div className='horizontal-center'>
					<iframe width='426px' height='240px' src={`https://www.youtube.com/embed/${this.state.vid}`} title='YouTube video player' frameBorder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowFullScreen={true}></iframe> 
					<hr />
					<h3>Processing Request</h3>
					<br />
					<div className='progress-wrapper'>
						<div className='progress-info'>
							<div className='progress-label'><span className='text-dark'>Downloading Video</span></div>
							<div className='progress-percentage'><span>{this.state.download}%</span></div>
						</div>
						<div className='progress loading-progress'>
							<div className='progress-bar loading-progress-bar progress-bar-striped bg-dark' style={{width: `${this.state.download}%`}} role='progressbar' aria-valuenow={this.state.download} aria-valuemin={0} aria-valuemax={100}></div>
						</div>
					</div>
					<div className='progress-wrapper'>
						<div className='progress-info'>
							<div className='progress-label'><span className='text-dark'>Sampling Frames</span></div>
							<div className='progress-percentage'><span>{this.state.sample}%</span></div>
						</div>
						<div className='progress loading-progress'>
							<div className='progress-bar loading-progress-bar progress-bar-striped bg-dark' style={{width: `${this.state.sample}%`}} role='progressbar' aria-valuenow={this.state.sample} aria-valuemin={0} aria-valuemax={100}></div>
						</div>
					</div>
					<div className='progress-wrapper'>
						<div className='progress-info'>
							<div className='progress-label'><span className='text-dark'>Running Scene Detection</span></div>
							<div className='progress-percentage'><span>{this.state.scene}%</span></div>
						</div>
						<div className='progress loading-progress'>
							<div className='progress-bar loading-progress-bar progress-bar-striped bg-dark' style={{width: `${this.state.scene}%`}} role='progressbar' aria-valuenow={this.state.scene} aria-valuemin={0} aria-valuemax={100}></div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default Loading;