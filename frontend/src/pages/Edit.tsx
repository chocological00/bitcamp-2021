import React, { MouseEvent as ReactMouseEvent } from 'react';
import { WebSocketDone } from '../common/types';

interface IState {
	totalDuration: number;
	sectionStart: number;
	sectionEnd: number;
	largePreview: string;
	tempLargePreview: string|null;
	thumbnails: string[];
	textbox: string;
	textBoxEditable: boolean;
	globalCursors: number[];
	sectionCursors: number[];
	hoverSection: number|null;
	mode: null|'globaladd'|'globaldel'|'sectionadd'|'sectiondel';
	sectionDict: { [key: number]: [Array<number>, string] } // sectionStart, keyFrames, text

	wsId: string;
	vidId: string;
	s3Base: string;
	frameCount: number;
	captions: WebSocketDone["captions"];
	scenes: number[];
}

class Edit extends React.Component<unknown, IState> {
	constructor(props: unknown) {
		super(props);

		// sanity check
		const wsId = window.sessionStorage.getItem('websocket-id');
		const vidId = window.sessionStorage.getItem('video-id');
		const s3Base = window.sessionStorage.getItem('s3-base');
		const s_frameCount = window.sessionStorage.getItem('frame-count');
		const s_captions = window.sessionStorage.getItem('captions');
		const s_scenes = window.sessionStorage.getItem('scenes');
		if (!(wsId && vidId && s3Base && s_frameCount && s_captions && s_scenes)) (window.location.href = '/')
		const frameCount = parseInt(s_frameCount as string);
		const captions: WebSocketDone["captions"] = JSON.parse(s_captions as string);
		const scenes: number[] = JSON.parse(s_scenes as string);

		const sectionDict: { [key: number]: [Array<number>, string] } = {};
		for (const i in scenes) {
			const curScene = scenes[i];
			const nextScene = scenes[parseInt(i) + 1];
			const text = captions.filter(e => e.start > curScene && e.start < nextScene).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
			sectionDict[curScene] = [[Math.floor((curScene + nextScene) / 2)], text];
		}

		this.state = {
			totalDuration: scenes[scenes.length - 1],
			sectionStart: scenes[0],
			sectionEnd: scenes[1],
			largePreview: `${s3Base}/${vidId}_${Math.floor(scenes[1] / 2)}.jpg`,
			tempLargePreview: null,
			thumbnails: [`${s3Base}/${vidId}_${Math.floor(scenes[1] / 2)}.jpg`],
			textbox: sectionDict[0][1],
			textBoxEditable: false,
			globalCursors: scenes.slice(2), // excludes current sectionStart and sectionEnds
			sectionCursors: [Math.floor(scenes[1] / 2)],
			hoverSection: null,
			mode: null,
			sectionDict: sectionDict,

			wsId: wsId as string,
			vidId: vidId as string,
			s3Base: s3Base as string,
			frameCount: frameCount,
			captions: captions,
			scenes: scenes
		}

		this.draggableSectionStart = this.draggableSectionStart.bind(this);
		this.draggableSectionEnd = this.draggableSectionEnd.bind(this);
		this.addNewSectionHandler = this.addNewSectionHandler.bind(this);
		this.addToGlobalCursors = this.addToGlobalCursors.bind(this);
		this.addNewSectionClickHandler = this.addNewSectionClickHandler.bind(this);
		this.deleteFromGlobalCursors = this.deleteFromGlobalCursors.bind(this);
		this.deleteSectionHandler = this.deleteSectionHandler.bind(this);
		this.deleteSectionClickHandler = this.deleteSectionClickHandler.bind(this);
		this.onGlobalTimelineHover = this.onGlobalTimelineHover.bind(this);
		this.onGlobalTimelineUnhover = this.onGlobalTimelineUnhover.bind(this);
		this.changeCurrentSection = this.changeCurrentSection.bind(this);
		this.mouseEventLocationToTimeStampGlobal = this.mouseEventLocationToTimeStampGlobal.bind(this);

		this.mouseEventLocationToTimeStampSection = this.mouseEventLocationToTimeStampSection.bind(this);
		this.addToSectionCursors = this.addToSectionCursors.bind(this);
		this.deleteFromSectionCursors = this.deleteFromSectionCursors.bind(this);
		this.addNewImageClickHandler = this.addNewImageClickHandler.bind(this);
		this.addNewImageHandler = this.addNewImageHandler.bind(this);
		this.deleteImageClickHandler = this.deleteImageClickHandler.bind(this);
		this.deleteImageHandler = this.deleteImageHandler.bind(this);
		this.onSectionTimelineHover = this.onSectionTimelineHover.bind(this);
		this.onSectionTimelineUnhover = this.onSectionTimelineUnhover.bind(this);

		this.textAreaEditorMode = this.textAreaEditorMode.bind(this);
		this.textAreaOriginalMode = this.textAreaOriginalMode.bind(this);

		this.findSubtitlesBetween = this.findSubtitlesBetween.bind(this);

		this.publish = this.publish.bind(this);
	}

	genS3(n: number) {
		return `${this.state.s3Base}/${this.state.vidId}_${Math.floor(n)}.jpg`
	}

	findSubtitlesBetween(start: number, end: number) {
		return this.state.captions.filter(e => e.start > start && e.start < end).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
	}

	formatSeconds(seconds: number) {
		let str = ''
		if (seconds >= 3600) {
			const hrs = Math.floor(seconds / 3600);
			str = str + (hrs < 10 ? `0${hrs}:` : `${hrs}:`);
		}
		if (seconds >= 60) {
			const mins = Math.floor((seconds % 3600) / 60);
			str = str + (mins < 10 ? `0${mins}:` : `${mins}:`);
		}
		else str = '00:';

		const secs = seconds % 60;
		str = str + (secs < 10 ? `0${secs}` : `${secs}`);
		return str;
	}

	draggableSectionStart(e: ReactMouseEvent) {
		e = e || window.event;
		e.preventDefault();

		const og_image = this.state.largePreview;

		const width = (document.getElementById('global-timeline') as HTMLElement).clientWidth;

		const lowerBound = Math.max(...this.state.globalCursors.filter(e => e < this.state.sectionStart));
		const upperBound = this.state.sectionCursors ?
			Math.min(...this.state.sectionCursors) :
			this.state.sectionEnd;

		let lastX = e.clientX;
		let diffX = 0;

		const dragEvent = (e: MouseEvent) => {
			e = e || window.event;
			diffX = e.clientX - lastX;
			lastX = e.clientX;
			const diffSec = Math.round(diffX * this.state.totalDuration / width);
			const newSec = this.state.sectionStart + diffSec;
			if (newSec > lowerBound && newSec < upperBound) {
				this.setState({ sectionStart: newSec });
				this.setState({ largePreview: this.genS3(newSec) }); // TODO:
			}
		}
		const clearDrag = () => {
			document.onmouseup = null;
			document.onmousemove = null;
			this.setState({ largePreview: og_image });
		}
		
		document.onmouseup = clearDrag;
		document.onmousemove = dragEvent;
	}

	draggableSectionEnd(e: ReactMouseEvent) {
		e = e || window.event;
		e.preventDefault();

		const og_image = this.state.largePreview;

		const width = (document.getElementById('global-timeline') as HTMLElement).clientWidth;

		const upperBound = Math.min(...this.state.globalCursors.filter(e => e > this.state.sectionEnd));
		const lowerBound = this.state.sectionCursors ?
			Math.max(...this.state.sectionCursors) :
			this.state.sectionStart;

		let lastX = e.clientX;
		let diffX = 0;

		const dragEvent = (e: MouseEvent) => {
			e = e || window.event;
			diffX = e.clientX - lastX;
			lastX = e.clientX;
			const diffSec = Math.round(diffX * this.state.totalDuration / width);
			const newSec = this.state.sectionEnd + diffSec;
			if (newSec > lowerBound && newSec < upperBound) {
				this.setState({ sectionEnd: newSec });
				this.setState({ largePreview: this.genS3(newSec) }); // TODO:
			}
		}
		const clearDrag = () => {
			document.onmouseup = null;
			document.onmousemove = null;
			this.setState({ largePreview: og_image });
		}
		
		document.onmouseup = clearDrag;
		document.onmousemove = dragEvent;
	}

	mouseEventLocationToTimeStampGlobal(e: MouseEvent|ReactMouseEvent) {
		const width = (document.getElementById('global-timeline') as HTMLElement).clientWidth;
		const start = (document.getElementById('global-timeline') as HTMLElement).getBoundingClientRect().left;
		return Math.floor((e.clientX - start) * this.state.totalDuration / width);
	}

	onGlobalTimelineHover(e: ReactMouseEvent) {
		const curTime = this.mouseEventLocationToTimeStampGlobal(e);
		const arr = [this.state.sectionStart, this.state.sectionEnd].concat(this.state.globalCursors);
		const lowerBound = Math.max(...arr.filter(e => e < curTime));
		this.setState({ hoverSection: lowerBound });
	}

	onGlobalTimelineUnhover(e: ReactMouseEvent) {
		this.setState({ hoverSection: null });
	}

	changeCurrentSection(e: ReactMouseEvent) {
		const curTime = this.mouseEventLocationToTimeStampGlobal(e);
		const arr = [this.state.sectionStart, this.state.sectionEnd].concat(this.state.globalCursors);
		const lowerBound = Math.max(...arr.filter(e => e < curTime));
		const upperBound = Math.min(...arr.filter(e => e > curTime));
		this.addToGlobalCursors(this.state.sectionStart);
		this.addToGlobalCursors(this.state.sectionEnd);
		this.setState({ sectionStart: lowerBound });
		this.setState({ sectionEnd: upperBound });
		this.deleteFromGlobalCursors(lowerBound);
		this.deleteFromGlobalCursors(upperBound);
		if (!this.state.sectionDict[lowerBound]) {
			const sectionDict = this.state.sectionDict;
			const text = this.state.captions.filter(e => e.start > lowerBound && e.start < upperBound).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
			sectionDict[lowerBound] = [[Math.floor((lowerBound + upperBound) / 2)], text];
			this.setState({ sectionDict: sectionDict });
		}
		this.setState({ largePreview: this.genS3(this.state.sectionDict[lowerBound][0][0]) });
		this.setState({ textbox: this.findSubtitlesBetween(lowerBound, upperBound) })
		this.setState({ sectionCursors: this.state.sectionDict[lowerBound][0] });
		this.setState({ thumbnails: this.state.sectionDict[lowerBound][0].map(e => this.genS3(e)) });
	}

	addNewSectionClickHandler(e: ReactMouseEvent) {
		const toAdd = this.mouseEventLocationToTimeStampGlobal(e);
		this.addToGlobalCursors(toAdd);
		this.setState({ mode: null });
	} 

	addNewSectionHandler() {
		if (this.state.mode === null) this.setState({ mode: 'globaladd' });
		else if (this.state.mode === 'globaladd') this.setState({ mode: null });
		else return;
	}

	deleteSectionClickHandler(e: ReactMouseEvent) {
		const toDelete = this.mouseEventLocationToTimeStampGlobal(e);
		if (this.deleteFromGlobalCursors(toDelete))
			this.setState({ mode: null });
	}

	deleteSectionHandler() {
		if (this.state.mode === null) this.setState({ mode: 'globaldel' });
		else if (this.state.mode === 'globaldel') this.setState({ mode: null });
		else return;
	}

	addToGlobalCursors(n: number) {
		if (this.state.globalCursors.includes(n)) return;
		const arr = this.state.globalCursors;
		arr.push(n);
		arr.sort();

		this.setState({ globalCursors: arr })
	}

	deleteFromGlobalCursors(n: number): boolean {
		if (!this.state.globalCursors.includes(n)) return false;
		const arr = this.state.globalCursors;
		const i = arr.indexOf(n);
		if (i > -1) arr.splice(i, 1);

		this.setState({ globalCursors: arr });
		return true;
	}

	////////////////////////////////////////////////////////////////
	mouseEventLocationToTimeStampSection(e: MouseEvent|ReactMouseEvent) {
		const width = (document.getElementById('section-timeline') as HTMLElement).clientWidth;
		const start = (document.getElementById('section-timeline') as HTMLElement).getBoundingClientRect().left;
		return Math.round((e.clientX - start) * (this.state.sectionEnd - this.state.sectionStart) / width) + this.state.sectionStart;
	}

	addToSectionCursors(n: number) {
		if (this.state.sectionCursors.includes(n)) return;
		const arr = this.state.sectionCursors;
		arr.push(n);
		arr.sort();

		const sectionData = this.state.sectionDict;
		if (!sectionData[this.state.sectionStart]) {
			const text = this.state.captions.filter(e => e.start > this.state.sectionStart && e.start < this.state.sectionEnd).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
			sectionData[this.state.sectionStart] = [[Math.floor((this.state.sectionStart + this.state.sectionEnd) / 2)], text];
			this.setState({ sectionDict: sectionData });
		}

		const text = this.state.captions.filter(e => e.start > this.state.sectionStart && e.start < this.state.sectionEnd).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
		sectionData[this.state.sectionStart] = [[Math.floor((this.state.sectionStart + this.state.sectionEnd) / 2)], text];
		this.setState({ sectionDict: sectionData });

		this.setState({ thumbnails: arr.map(e => this.genS3(e)) });

		this.setState({ sectionCursors: arr })
	}

	deleteFromSectionCursors(n: number): boolean {
		if (!this.state.sectionCursors.includes(n)) return false;
		const arr = this.state.sectionCursors;
		const i = arr.indexOf(n);
		if (i > -1) arr.splice(i, 1);

		this.setState({ thumbnails: arr.map(e => this.genS3(e)) });

		this.setState({ sectionCursors: arr });
		return true;
	}

	addNewImageClickHandler(e: ReactMouseEvent) {
		const toAdd = this.mouseEventLocationToTimeStampSection(e);
		this.addToSectionCursors(toAdd);
		this.setState({ mode: null });
	} 

	addNewImageHandler() {
		if (this.state.mode === null) this.setState({ mode: 'sectionadd' });
		else if (this.state.mode === 'sectionadd') this.setState({ mode: null });
		else return;
	}

	deleteImageClickHandler(e: ReactMouseEvent) {
		const toDelete = this.mouseEventLocationToTimeStampSection(e);
		if (this.deleteFromSectionCursors(toDelete))
			this.setState({ mode: null });
	}

	deleteImageHandler() {
		if (this.state.mode === null) this.setState({ mode: 'sectiondel' });
		else if (this.state.mode === 'sectiondel') this.setState({ mode: null });
		else return;
	}

	onSectionTimelineHover(e: ReactMouseEvent) {
		const curTime = this.mouseEventLocationToTimeStampSection(e);
		this.setState({ tempLargePreview: this.state.largePreview });
		this.setState({ largePreview: `${this.state.s3Base}/${this.state.vidId}_${curTime}.jpg` }); // TODO:
	}

	onSectionTimelineUnhover(e: ReactMouseEvent) {
		this.setState({ largePreview: this.genS3(this.state.sectionCursors[0]) })
	}

	textAreaEditorMode() {

	}

	textAreaOriginalMode() {

	}

	publish() {
		const arr = [this.state.sectionStart, this.state.sectionEnd].concat(this.state.globalCursors).sort();
		const sectionDict: { [key: number]: [Array<number>, string] } = {};
		for (const i in arr.slice(0, arr.length - 1)) {
			const curScene = arr[i];
			const nextScene = arr[parseInt(i) + 1];
			const text = this.state.captions.filter(e => e.start > curScene && e.start < nextScene).map(e => e.text).reduce((acc, e) => `${acc} ${e}`, '');
			sectionDict[curScene] = [[Math.floor((curScene + nextScene) / 2)], text];
		}
		window.sessionStorage.setItem('finished', JSON.stringify(sectionDict));
		window.location.href = `/show/${this.state.vidId}`;
	}

	render() {
		const globalTimeline = document.getElementById('global-timeline');
		const sectionTimeline = document.getElementById('section-timeline');
		let gtl_width: number|null = null, stl_width: number|null = null;
		if (globalTimeline && sectionTimeline) {
			gtl_width = globalTimeline.clientWidth;
			stl_width = sectionTimeline.clientWidth;
		}

		const start = Math.floor(this.state.sectionStart / this.state.totalDuration * (gtl_width as number));
		const end = Math.floor(this.state.sectionEnd / this.state.totalDuration * (gtl_width as number));

		return (
			<div className='fullscreen-container grid-editor-container'>
				<div className='controls'>
					<div className='progress-wrapper timeline' id='global-timeline' style={{cursor: `${this.state.mode === 'globaladd' || this.state.mode === 'globaldel' ? 'crosshair' : (this.state.mode === null ? 'pointer' : 'auto')}`}} onClick={this.state.mode === 'globaladd' ? this.addNewSectionClickHandler : (this.state.mode === 'globaldel' ? this.deleteSectionClickHandler : (this.state.mode === null ? this.changeCurrentSection : (() => {})))} onMouseMove={this.state.mode === null ? this.onGlobalTimelineHover : (() => {})} onMouseLeave={this.state.mode === null ? this.onGlobalTimelineUnhover : (() => {})}>
						<div className='progress-info'>
							<div className='progress-label'><span className='text-dark'>Global Timeline</span></div>
							<div className='progress-percentage'><span>{this.formatSeconds(this.state.totalDuration)}</span></div>
						</div>
						<div className='progress no-margin-bottom'></div>
						{this.state.globalCursors.filter(sec => sec !== 0 && sec !== this.state.totalDuration).map((sec, i) =>
							<div className='marker' key={i} style={{top: `30px`, left: `${Math.floor(sec / this.state.totalDuration * (gtl_width as number)) - 1.5}px`}}></div>
						)}
						<div className='marker-with-timestamp shadow-soft border border-light rounded flex-center-container' id='section-start' style={{top: `30px`, left: `${start-16}px`}} onMouseDown={this.draggableSectionStart}><span className='editor-cursor-timestamp'>{this.formatSeconds(this.state.sectionStart)}</span></div>
						<div className='marker-with-timestamp shadow-soft border border-light rounded flex-center-container' id='section-end' style={{top: `30px`, left: `${end-16}px`}} onMouseDown={this.draggableSectionEnd}><span className='editor-cursor-timestamp'>{this.formatSeconds(this.state.sectionEnd)}</span></div>
						<div className='timeline-overlay-blue' style={{top: '34px', left: `${start}px`, width: `${end-start}px`}}></div>
						{this.state.hoverSection && this.state.hoverSection !== this.state.sectionStart ? 
							<div className='timeline-overlay-purple' style={{top: '34px', left: `${Math.floor(this.state.hoverSection / this.state.totalDuration * (gtl_width as number))}px`, width: `${Math.floor((Math.min(...[this.state.sectionStart, this.state.sectionEnd].concat(this.state.globalCursors).filter(e => e > (this.state.hoverSection as number))) - this.state.hoverSection) / this.state.totalDuration * (gtl_width as number))}px`}}></div>
							: <div style={{visibility: 'hidden'}}></div>}
					</div>
					<button className={`btn btn-icon-only btn-pill spaced-button ${this.state.mode === 'globaladd' ? 'active btn-danger' : 'btn-primary'}`} onClick={this.addNewSectionHandler} type='button' aria-label='Add new section separator' data-toggle='tooltip' data-placement='bottom' title='Add new section separator'>
						<span aria-hidden='true' className='fas fa-i-cursor fa-lg'></span>
					</button>
					<button className={`btn btn-icon-only btn-pill spaced-button ${this.state.mode === 'globaldel' ? 'active btn-danger' : 'btn-primary'}`} onClick={this.deleteSectionHandler} type='button' aria-label='Remove existing section separator' data-toggle='tooltip' data-placement='bottom' title='Remove existing section separator'>
						<span aria-hidden='true' className='fas fa-times fa-lg'></span>
					</button>
					<div style={{backgroundColor: '#31344b', width: '2px', marginLeft: '1rem', height: '60%', opacity: '20%'}}></div>
					<button className='btn btn-icon-only btn-primary btn-pill spaced-button' onClick={this.publish} type='button' aria-label='Save and Publish' data-toggle='tooltip' data-placement='bottom' title='Save and Publish'>
						<span aria-hidden='true' className='fas fa-save fa-lg'></span>
					</button>
				</div>
				<div className='inner-editor-container'>
					<div className='editor-ui-container'>
						<div className='editor-ui-text'>Summary Images</div>
						<div className='flex-summary-image-container'>
							<img className='carousel shadow-soft border border-light rounded' src={this.state.largePreview}></img>
							<div className='existing-images carousel shadow-soft p-3 rounded'>
								<div className='image-selector' id='im-sel'>
									{this.state.thumbnails.map((url, i) =>
										<img src={url} key={i} className='thumbnail rounded'></img>
									)}
								</div>
							</div>
						</div>
					</div>
					<div className='flex-center-container'><div className='vertical-line'></div></div>
					<div className='editor-ui-container'>
						<div className='editor-ui-text'>Transcript</div>
						<div className='flex-transcript-container'>
							<div className='summary-text' id='text-summary'>
								<textarea className='form-control' id='textarea' style={{ resize: 'none' }} readOnly={!this.state.textBoxEditable}></textarea>
							</div>
							<div className='flex-center-container margin-up'>
								<div className='btn-group btn-group-toggle' data-toggle='buttons'>
									{/* <label className='btn' onClick={this.textAreaEditorMode}>
										<input type='radio' name='text-mode' id='summarized' autoComplete='off' />Editor
									</label> */}
									<label className='btn active' onClick={this.textAreaOriginalMode}>
										<input type='radio' name='text-mode' id='fulltext' autoComplete='off' />Original Transcript
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className='controls'>
					<div className='progress-wrapper timeline' id='section-timeline' style={{cursor: `${this.state.mode === 'sectionadd' || this.state.mode === 'sectiondel' ? 'crosshair' : 'auto'}`}} onClick={this.state.mode === 'sectionadd' ? this.addNewImageClickHandler : (this.state.mode === 'sectiondel' ? this.deleteImageClickHandler : (() => {}))} onMouseMove={this.onSectionTimelineHover} onMouseLeave={this.onSectionTimelineUnhover}>
						<div className='progress no-margin-top less-margin-bottom'></div>
						<div className='progress-info no-margin-top no-margin-bottom'>
							<div className='progress-label'><span className='text-dark'>Section Timeline</span></div>
							<div className='progress-percentage'><span>{this.formatSeconds(this.state.sectionEnd - this.state.sectionStart)}</span></div>
						</div>
						{this.state.sectionCursors.map((sec, i) =>
							<div className='marker' key={i} style={{bottom: `26px`, left: `${Math.floor((sec - this.state.sectionStart) / (this.state.sectionEnd - this.state.sectionStart) * (stl_width as number)) - 1.5}px`}}></div>
						)}
					</div>
					<button className={`btn btn-icon-only btn-primary btn-pill spaced-button ${this.state.mode === 'sectionadd' ? 'active btn-danger': 'btn-primary'}`} onClick={this.addNewImageHandler} type='button' aria-label='Add new summary image' data-toggle='tooltip' data-placement='top' title='Add new summary image'>
						<span aria-hidden='true' className='fas fa-plus fa-lg'></span>
					</button>
					<button className={`btn btn-icon-only btn-primary btn-pill spaced-button ${this.state.mode === 'sectiondel' ? 'active btn-danger': 'btn-primary'}`} onClick={this.deleteImageHandler} type='button' aria-label='Remove existing summary image' data-toggle='tooltip' data-placement='top' title='Remove existing summary image'>
						<span aria-hidden='true' className='fas fa-minus fa-lg'></span>
					</button>
				</div>
			</div>
		)
	}

	componentDidMount() {
		($('[data-toggle="tooltip"]') as any).tooltip();
		(document.getElementById('im-sel') as HTMLElement).style.height = `${(document.getElementById('im-sel') as HTMLElement).clientHeight}px`;
		(document.getElementById('textarea') as HTMLElement).style.height = `${(document.getElementById('text-summary') as HTMLElement).clientHeight}px`;

		(document.getElementById('textarea') as HTMLInputElement).value = this.state.textbox;
		
		// TODO: eventhandler to window resize

	}

	componentDidUpdate() {
		($('[data-toggle="tooltip"]') as any).tooltip();
		(document.getElementById('textarea') as HTMLInputElement).value = this.state.textbox;
	}
}

export default Edit;