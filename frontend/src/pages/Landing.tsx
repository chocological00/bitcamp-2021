import React, { KeyboardEvent, MouseEvent } from 'react';
import axios from 'axios';
import { validateURL } from '../common/utils';
import { RequestBody, RequestResponse } from '../common/types';

class Landing extends React.Component<unknown, unknown> {
	constructor(props: unknown) {
		super(props);
		this.blogify = this.blogify.bind(this);
		this.keyPress = this.keyPress.bind(this);
	}

	openYouTube() {
		window.location.href = 'https://www.youtube.com'
	}

	async blogify() {
		const link = (document.getElementById('youtube-url') as HTMLInputElement).value as string | null;
		if (!link) {
			this.notValid();
			return;
		}
		const valid = validateURL(link);
		if (!valid) {
			this.notValid();
			return;
		}

		// TODO: API Specs - 400 if invalid - get video id too
		const body: RequestBody = {
			url: link
		}
		
		const res = await axios.post<RequestResponse>(`/request`, body);

		window.sessionStorage.setItem('websocket-id', res.data.uuid);
		window.sessionStorage.setItem('video-id', res.data.video_id);
		window.location.href = '/loading'
	}

	notValid() {
		alert('Invalid URL! Please check if your link is a valid youtube video.');
	}

	keyPress(e: KeyboardEvent) {
		if (e.key === 'Enter') {
			e.preventDefault();
			this.blogify();
		}
	}

	writeToClipboard(url: string) {
		const wtc = (e: MouseEvent) => {
			e.preventDefault();
			navigator.clipboard.writeText(url).then(() => { alert('Copied to clipboard!') }, () => { alert(`Could not copy to clipboard! URL is ${url}`)});
		}
		return wtc;
	}

	render() {
		return (
			<div className='fullscreen-container flex-center-container'>
				<div className='horizontal-center'>
					<h1><b>blogify.tech</b></h1>
					<h6>Turn any YouTube video</h6>
					<h6>into a <b>Blog Post</b></h6>
					<br />
					<div className='input-group' style={{ width: '768px' }}>
						<div className='input-group-prepend' onClick={this.openYouTube} style={{ cursor: 'pointer' }}>
							<span className='input-group-text'><i className='fab fa-youtube fa-lg'></i></span>
						</div>
						<input type='text' className='form-control' id='youtube-url' onKeyPress={this.keyPress} placeholder='Enter the Youtube Link to blogify!' aria-label='Enter youtube link to summarize' />
						<div className='input-group-append' onClick={this.blogify}>
							<button className='btn btn-gray text-light' type='button'>GO!</button>
						</div>
					</div>
					<br />
					<br />
					<h6><b>This is a prototype version!</b></h6>
					<ul>
						<li><b><i>DOES NOT WORK ON MOBILE - PLEASE USE FULLSCREEN PC</i></b></li>
						<li>Works best with English video with subtitles</li>
						<li>Videos too long might have trouble editing</li>
						<li>Don&apos;t resize window while in editor</li>
						<li>AV1 encoded videos may fail silently</li>
						<li>
							Here are some videos we tested with! (click to copy):&nbsp;
							<a href='#' onClick={this.writeToClipboard('https://www.youtube.com/watch?v=tUX-frlNBJY')}>1</a>&nbsp;
							<a href='#' onClick={this.writeToClipboard('')}>2</a>
						</li>
					</ul>
					{/* <br/>
					<br/>
					<h6><b>Sadly this service is not available currently. The current backend is inefficient, and the costs of running the server are too much (running out of free credit!)</b></h6>
					<h6><b>We will be tring to fix this, and make it available again.</b></h6> */}
				</div>
			</div>
		);
	}
}

export default Landing;
