# Build frontend, backend
FROM node:14-alpine AS builder
LABEL stage=builder

WORKDIR /usr/src/app

COPY ./common ./common/
RUN mkdir -p frontend/src/common && cp common/types.d.ts frontend/src/common/types.ts && cp common/utils.ts frontend/src/common/utils.ts

COPY ./frontend/tsconfig*.json ./frontend/
COPY ./frontend/package*.json ./frontend/yarn.lock ./frontend/
COPY ./frontend/.eslintrc* ./frontend/
RUN cd frontend && yarn install && cd ..
COPY ./frontend/public ./frontend/public/
COPY ./frontend/src ./frontend/src/
RUN cd frontend && yarn run build && cd ..

COPY ./backend/tsconfig*.json ./backend/
COPY ./backend/package*.json ./backend/yarn.lock ./backend/
RUN cd backend && yarn install && cd ..
COPY ./backend/src ./backend/src/
RUN cd backend && yarn build

# install prod packages, copy builds over
FROM python:3.8-slim
WORKDIR /app
ENV NODE_ENV=production

COPY ./backend/package*.json ./backend/yarn.lock ./backend/requirements.txt ./backend/

RUN apt-get update && apt-get install -y curl ffmpeg \
        && curl -sL https://deb.nodesource.com/setup_14.x | bash \
        && apt-get install -y nodejs && npm i -g yarn \
        && pip3 install -r ./backend/requirements.txt

# IF RUNNING ON ARM64: USE THE FOLLOWING LINES
#COPY ./backend/nnsplit/nnsplit-0.5.7_post0-cp38-cp38-linux_aarch64.whl ./backend/nnsplit-0.5.7_post0-cp38-cp38-linux_aarch64.whl
#RUN pip3 install ./backend/nnsplit-0.5.7_post0-cp38-cp38-linux_aarch64.whl

# IF RUNNING x86_64 OR AMD64: USE THE FOLLOWING LINE
RUN pip3 install nnsplit

RUN cd backend && yarn install && cd ..

COPY --from=builder /usr/src/app/backend/bin ./backend/bin/
COPY ./backend/scripts ./backend/scripts/
COPY --from=builder /usr/src/app/frontend/build ./frontend/build/

WORKDIR /app/backend

CMD [ "node", "./bin/backend/src/index.js" ]
