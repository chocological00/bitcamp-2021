"""
FROM https://gist.github.com/glasslion/b2fcad16bc8a9630dbd7a945ab5ebf5e
CREDIT TO glasslion on Github

Convert YouTube subtitles(vtt) to human readable text.
Download only subtitles from YouTube with youtube-dl:
youtube-dl  --skip-download --convert-subs vtt <video_url>
Note that default subtitle format provided by YouTube is ass, which is hard
to process with simple regex. Luckily youtube-dl can convert ass to vtt, which
is easier to process.
To conver all vtt files inside a directory:
find . -name "*.vtt" -exec python vtt2text.py {} \;
"""

import sys
import re


def remove_tags(text):
    """
    Remove vtt markup tags
    """
    tags = [
        r'</c>',
        r'<c(\.color\w+)?>',
        r'<\d{2}:\d{2}:\d{2}\.\d{3}>',

    ]

    for pat in tags:
        text = re.sub(pat, '', text)

    # extract timestamp, only kep HH:MM:SS
    text = re.sub(
        r'(\d{2}:\d{2}:\d{2})\.\d{3} --> .* align:start position:0%',
        r'\g<1>',
        text
    )


    text = re.sub(r'^\s+$', '', text, flags=re.MULTILINE)
    return text

def remove_header(lines):
    """
    Remove vtt file header
    """
    pos = -1
    for mark in ('##', 'Language: en',):
        if mark in lines:
            pos = lines.index(mark)
    lines = lines[pos+1:]
    return lines


def merge_duplicates(lines):
    """
    Remove duplicated subtitles. Duplacates are always adjacent.
    """
    last_timestamp = ''
    last_cap = ''
    for line in lines:
        if line == "":
            continue
        if re.match('^\d{2}:\d{2}:\d{2}$', line):
            if line != last_timestamp:
                yield line
                last_timestamp = line
        else:
            if line != last_cap:
                yield line
                last_cap = line


def merge_short_lines(lines):
    buffer = ''
    for line in lines:
        if line == "" or re.match('^\d{2}:\d{2}:\d{2}$', line):
            yield '\n' + line
            continue

        if len(line+buffer) < 80:
            buffer += ' ' + line
        else:
            yield buffer.strip()
            buffer = line
    yield buffer


def main():

    from nnsplit import NNSplit
    splitter = NNSplit.load("en")

    vtt_file_name = sys.argv[1]
    txt_name =  re.sub(r'.vtt$', '.txt', vtt_file_name)
    with open(vtt_file_name) as f:
        text = f.read()
    if (text.find("align:start position:0%") != -1):
        text = remove_tags(text)
        lines = text.splitlines()
        lines = remove_header(lines)
        lines = merge_duplicates(lines)
        lines = list(lines)
        new_lines = list()
        i = 0
        while i+1 < len(lines):
            if not (re.match(r'\d\d:\d\d:\d\d', lines[i]) and re.match(r'\d\d:\d\d:\d\d', lines[i+1])):
                new_lines.append(lines[i])
            i += 1
        new_lines.append(lines[i])
        lines = new_lines
        #lines = merge_short_lines(lines)
        #lines = list(lines)
        old_lines = lines.copy()

        only_text = ''
        for line in lines:
            if not re.match(r'\d\d:\d\d:\d\d', line):
                only_text = only_text + ' ' + line
        #print(only_text)
        splits = splitter.split([only_text])[0]
        # a `Split` can be iterated over to yield smaller splits or stringified with `str(...)`.
        sentences = list(splits)
        #for line in old_lines:
        #    print(line)
        currLine = 0
        currSentence = 0
        lineLoc = 0
        text = str(sentences[0]).strip()
        while currSentence < len(sentences)-1 and currLine < len(lines):
            if re.match(r'\d\d:\d\d:\d\d', lines[currLine]):
                currLine += 1
                continue

            #print("Sent " + text)
            #print("Line " + str(lines[currLine])[lineLoc:])
            lineLen = len(lines[currLine])-lineLoc
            if len(text) > lineLen:
                text = text[lineLen:].lstrip()
                currLine += 1
                lineLoc = 0
            elif len(text) < lineLen:
                old_lines[currLine] = lines[currLine][0:len(text)+lineLoc] + '.' + lines[currLine][len(text)+lineLoc:]
                currSentence += 1
                lineLoc += len(text)
                if lines[currLine][lineLoc] == ' ':
                    lineLoc += 1
                text = str(sentences[currSentence]).strip()
            else:
                old_lines[currLine] = lines[currLine] + '.'
                currSentence += 1
                currLine += 1
                lineLoc = 0
                text = str(sentences[currSentence]).strip()

        times = list()
        i = 0
        while i < len(old_lines):
            if re.match(r'\d\d:\d\d:\d\d', old_lines[i]):
                times.append(i)
            i += 1
            #print(i)
        
        print("WEBVTT\nKind: captions\nLanguage: en\n")
        i = 0
        while i+1 < len(times):
            curr = times[i]
            next = times[i+1]
            print(old_lines[curr] + ".000 --> " + old_lines[next] + ".000")
            j = curr + 1
            while j < next:
                print(old_lines[j])
                j += 1
            print()

            i += 1
        search = re.search(r"(\d\d):(\d\d):(\d\d)", old_lines[times[i]])
        if search:
            print(old_lines[times[i]] + ".000 --> " + search.group(1) + ":" + search.group(2) + ":" + str(int(search.group(3))+4) + ".000")
        print(old_lines[times[i]+1])
        print("\n")
    else:
        text = re.sub(r"&amp;gt;", '>', text, flags=re.MULTILINE)
        print(text)
    

    # with open(txt_name, 'w') as f:
    #     for line in lines:
    #         f.write(line)
    #         f.write("\n")



if __name__ == "__main__":
    main()
