import {ProgressStage, WebSocketDone} from '../../common/types';
import {subtitles} from './types';
import * as lib from './libs';
import fs from 'fs';
import { parse, Node } from 'subtitle';
import {Static} from 'runtypes';
import ffmpeg from 'ffmpeg';
import {S3Client, PutObjectCommand, PutObjectCommandOutput} from '@aws-sdk/client-s3';
import * as index from './index';
import chokidar from 'chokidar';
import readline from 'readline';
import { spawn } from 'child_process';
import {Readable} from 'stream';

const FRAME_PATTERN = /data\/frames\/.*\/(\d+).jpg/m;
const CSV_PATTERN = /\d+,\d+,(?:\d\d:\d\d:\d\d.\d{3}),\d+.\d+,\d+,(?:\d\d:\d\d:\d\d.\d{3}),(\d+.\d{3})/m;

export async function analyze(id: string, statusCb: (stage: ProgressStage, progress: number) => boolean): Promise<WebSocketDone>
{
	// CHECK EXISTING
	let existing_row = await index.db.getVideo(id);
	if (index.processing.has(id)) 
	{
		throw 'processing';
	} 
	else if (existing_row === null) 
	{
		index.processing.add(id);

		// DOWNLOAD VIDEO
		let data: lib.DLData;
		try 
		{
			data = await lib.youtube(id, statusCb);
		} 
		catch (err) 
		{
			// eslint-disable-next-line no-console
			console.log('DL error: ', err);
			throw 'download_error';
		}
		
		if (data.subs === null) 
		{
			throw 'no_subtitles';
		}
	
		// PROCESS SUBTITLES
		const stream = new Readable;
		// eslint-disable-next-line @typescript-eslint/no-empty-function
		stream._read = () => {}; // needed bc of some weird thing
		try 
		{
			void new Promise<void>((res, rej) => 
			{
				const py = spawn(index.config.lib.python, ['scripts/processSubtitles.py', data.subs as string]);
				//console.log(py.spawnargs);
			
				py.stdout.on('data', (data: Buffer) =>
				{
					const str = data.toString();
					stream.push(str);
				});
				// py.stderr.on('data', (data: Buffer) =>
				// {
				// 	const str = data.toString();
				// 	//console.log(str);
				// });
				py.on('error', (e) =>
				{
					// eslint-disable-next-line no-console
					console.log('Sub script error: ', e);
					rej('subtitle_error');
				});
				py.on('exit', (code) =>
				{
					if (code === 0)
					{
						res();
						stream.push(null);
					}
					else
					{
						// eslint-disable-next-line no-console
						console.log('Subs Exited with code ' + (code ? code.toString() : 'null'));
						rej('subtitle_error');
					}
				});
			});
		}
		catch (err) 
		{
			throw 'subtitle_error';
		}
	
		const allSubs: Static<typeof subtitles> = [];
	
		try 
		{
			await new Promise<void>((res, rej) =>
			{
				stream
					.pipe(parse())
					.on('data', (node: Node) =>
					{
						if (node.type === 'cue')
						{
							node.data.start /= 1000;
							node.data.end /= 1000;
							//node.data.text = node.data.text.replace(/&amp;gt;/g, '>').replace(/\\n/g, ' ').replace(/(<\d\d:\d\d:\d\d\.\d{3}>|<\/?c>)/g, '').trim();
							allSubs.push(node.data);
						}
					})
					.on('error', (err) => rej(err))
					.on('finish', () => res());
			});
		} 
		catch (err) 
		{
			// eslint-disable-next-line no-console
			console.log('Sub error: ', err);
			throw 'subtitle_error';
		}
	
		//console.log(allSubs);
	
	
		// PROCESS FRAMES
		const video = await new ffmpeg(data.video);
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const framesEstimate = video.metadata.duration!.seconds; //TODO: remove this 
		let currPercent = 0;
	
		const uploads: Promise<PutObjectCommandOutput>[] = [];
	
		const s3 = new S3Client(index.config.storage.s3);
		fs.mkdirSync(`./data/frames/${id}`, {recursive: true});
	
		const watcher = chokidar.watch(`data/frames/${id}`).on('add', (path) => 
		{
			//console.log(path);
			let m;
			if ((m = FRAME_PATTERN.exec(path)) !== null) 
			{
				const frame_num = m[1];
				const command = new PutObjectCommand({
					Body: fs.createReadStream(`data/frames/${id}/${frame_num}.jpg`),
					Bucket: 'axp6erdgdktz.compat.objectstorage.us-sanjose-1.oraclecloud.com',
					Key: `blogify/${id}_${frame_num}.jpg`,
					ACL: 'public-read'
				});
				const percent = Math.round(100*parseInt(frame_num)/(framesEstimate+2));
				//console.log(percent);
				if (percent - currPercent > 5) 
				{
					currPercent = percent;
					statusCb('frames', currPercent);
				}
				//console.log(`Uploading ${frame_num}`);
				setTimeout(() =>
				{
					uploads.push(s3.send(command));
				}, 500);
			}
		});
	
		//console.log(`Frames starting at ${Date.now()}`);

		const framePromise = lib.frames(data.video, id).then(async (num) => 
		{
			//console.log(`Frames done at ${Date.now()}`);
			await Promise.all(uploads);
			await watcher.close();
			statusCb('frames', 100);
			return num;
		})
			.catch((err) =>
			{
				// eslint-disable-next-line no-console
				console.log('Frame error: ', err);
				return Promise.reject('download_error');
			});
	
		// SCENE DETECTION
		const sceneTimings:number[] = [];
		sceneTimings.push(0);
	
		const scenePromise = lib.scenedetect(data.video, statusCb).then(async () =>
		{
			const rl = readline.createInterface({
				input: fs.createReadStream(`data/scenes/${id}-Scenes.csv`),
				crlfDelay: Infinity
			});
			for await (const line of rl) 
			{
				let m;
				if ((m = CSV_PATTERN.exec(line)) !== null) 
				{
					sceneTimings.push(Math.ceil(parseFloat(m[1])));
				}
			}
		
			statusCb('scenes', 100);
			//console.log(sceneTimings);
		})
			.catch((err) =>
			{
				// eslint-disable-next-line no-console
				console.log('Scene error: ', err);
				return Promise.reject('scene_error');
			});
		
		const promiseResult = await Promise.all([framePromise, scenePromise]);
		
		existing_row = {
			frames: promiseResult[0],
			frames_generated: true,
			subtitles: allSubs,
			vid_id: id,
			scenes: sceneTimings
		};
		
		await index.db.insertVideo(existing_row);
		index.processing.delete(id);
	}
	
	return {
		video_id: existing_row.vid_id,
		s3_base_url: index.config.storage.s3_external_endpoint,
		num_frames: existing_row.frames,
		captions: existing_row.subtitles,
		scenes: existing_row.scenes
	};
}