import {ProgressStage} from '../../common/types';
import {spawn} from 'child_process';
import * as index from './index';

const PROGRESS_REGEX = /\[download\]\s+(\d{0,2}\.\d+)%/m;
const FINISHED_REGEX = /\[download\]\s+(\d+)%/m;
const DL = /\[download\] Destination: .*\.(3gp|aac|flv|m4a|mp3|mp4|ogg|wav|webm)/m;
const EXISTING_REGEX = /\[download\] (.*) has already been downloaded/m;
const FILE_REGEX = /\[ffmpeg\] Merging formats into "(.*)"/m;
const SUB_REGEX = /\[info\] Writing video subtitles to: (.*)$/m;

const SCENE_PROGRESS = / *(\d+)%\|/m;
const SCENE_DONE = /Writing scene list to CSV file/m;

const FRAME_NUM = /frame=(\d+)/m;

export interface DLData {
	subs: string|null
	video: string
}

export function youtube(id: string, 
	statusCb: (stage: ProgressStage, progress: number) => boolean): Promise<DLData>
{
	const dl_url = `https://www.youtube.com/watch?v=${id}`;
	const yt_dl = spawn(index.config.lib.youtube_dl, ['--newline', '--force-ipv4', '--sub-lang', 'en,en-US,en-GB', '-f', 'bestvideo[vcodec!*=av01][height<=?720]+bestaudio/best', '--write-sub', '--write-auto-sub', '-o', 'data/videos/%(id)s.%(ext)s', dl_url]);
	//console.log(dl_url);
	return new Promise((res, rej) => 
	{
		let currentFormat = '';
		let progress = 0;
		let success = false;

		let subFile: string|null = null;
		let videoFile: string;

		yt_dl.stdout.on('data', (data: Buffer) =>
		{
			const str = data.toString();

			let m;
			if ((m = EXISTING_REGEX.exec(str)) !== null) 
			{
				statusCb('download', 100);
				success = true;
				videoFile = m[1];

			}
			else if ((m = FILE_REGEX.exec(str)) !== null) 
			{
				success = true;
				videoFile = m[1];
			} 
			else if ((m = SUB_REGEX.exec(str)) !== null) 
			{
				subFile = m[1];
			} 
			else if ((m = DL.exec(str) ) !== null && currentFormat === '') 
			{
				currentFormat = 'video';
			}
			else if ((m = DL.exec(str)) !== null && currentFormat === 'video') 
			{
				currentFormat = 'audio';
			}
			else if ((m = PROGRESS_REGEX.exec(str)) !== null) 
			{
				let newProgress = 0;
				if (currentFormat === 'video') 
				{
					newProgress = Math.round(.9*parseInt(m[1]));

				}
				else if (currentFormat === 'audio') 
				{
					const rounded = Math.round(90 + .1*parseInt(m[1]));
					newProgress = rounded === 100 ? 99 : rounded;

				}
				if (newProgress - progress > 5) 
				{
					progress = newProgress;
					if (statusCb('download', newProgress) === false) 
					{
						yt_dl.kill();
					}
				}
			}
			else if ((m = FINISHED_REGEX.exec(str)) !== null && currentFormat === 'audio') 
			{
				if (statusCb('download', parseInt(m[1])) === false)
				{
					yt_dl.kill();
				}
			}
			
		});
		yt_dl.on('error', (e) =>
		{
			rej(e);
		});
		yt_dl.on('exit', (code) =>
		{
			if (success) 
			{
				res({
					subs: subFile,
					video: videoFile
				});
			}
			else
			{
				rej('Exited with code ' + (code ? code.toString() : 'null'));
			}
		});
	});
}

export function frames(path: string, id: string): Promise<number>
{
	const detect = spawn(index.config.lib.ffmpeg, ['-i', path, '-progress', '-', '-vf', 'fps=1', `data/frames/${id}/%d.jpg`]);
	//const detect = spawn(index.config.lib.ffmpeg, ['-i', path, '-progress', '-', '-r', '1', `data/frames/${id}/%d.jpg`]);
	return new Promise((res, rej) => 
	{
		let frames = -1;

		detect.stdout.on('data', (data: Buffer) =>
		{
			const str = data.toString();
			if (str.search(/progress=end/) !== -1) 
			{
				let m;
				if ((m = FRAME_NUM.exec(str)) !== null) 
				{
					frames = parseInt(m[1]);
				}
			}
		});

		detect.on('error', (e) =>
		{
			// eslint-disable-next-line no-console
			console.log(e);
			rej(e);
		});
		detect.on('exit', (code) =>
		{
			//console.log("EXIT");
			if (frames !== -1) 
			{
				res(frames);
			}
			else
			{
				rej('Exited with code ' + (code ? code.toString() : 'null'));
			}
		});
	});
}

export function scenedetect(path: string, 
	statusCb: (stage: ProgressStage, progress: number) => boolean): Promise<void>
{
	const detect = spawn(index.config.lib.scenedetect, ['--input', path, '-fs', '3', '--output' ,'data/scenes', 'detect-content', 'list-scenes']);

	return new Promise((res, rej) => 
	{
		let progress = 0;
		let success = false;

		detect.stderr.on('data', (data: Buffer) =>
		{
			const str = data.toString();
			//console.log(data.toString());
			let m;
			if ((m = SCENE_PROGRESS.exec(str)) !== null) 
			{
				//console.log(m[1]);
				const newProgress = parseInt(m[1]);
				if (newProgress - progress > 5) 
				{
					progress = newProgress;
					if (statusCb('scenes', newProgress) === false) 
					{
						detect.kill();
					}
				}

			}
			else if ((m = SCENE_DONE.exec(str)) !== null) 
			{
				//console.log("DONE");
				success = true;
				if (statusCb('scenes', 100) === false) 
				{
					success = false;
					detect.kill();
				}
			}
		});
		detect.on('error', (e) =>
		{
			rej(e);
		});
		detect.on('exit', (code) =>
		{
			//console.log("EXIT");
			if (success) 
			{
				res();
			}
			else
			{
				rej('Exited with code ' + (code ? code.toString() : 'null'));
			}
		});
	});
}