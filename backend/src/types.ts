import { Record, Number, String, Array, Boolean } from 'runtypes';

export const CONFIG = Record({ 
	server: Record({
		port: Number.withConstraint(n => n > 0 || `${n} must be positive.`),
		addr: String,
		external_addr: String
	}),
	storage: Record({
		session_secret: String,
		postgres: Record({
			user: String,
			host: String,
			database: String,
			password: String,
			port: Number.withConstraint(n => n > 0 || `${n} must be positive.`)
		}),
		s3: Record({
			endpoint: String,
			bucketEndpoint: Boolean,
			credentials: Record({
				accessKeyId: String,
				secretAccessKey: String
			}),
			s3ForcePathStyle: Boolean,
			signatureVersion: String,
			region: String
		}),
		s3_external_endpoint: String
	}),
	lib: Record({
		youtube_dl: String,
		scenedetect: String,
		python: String,
		ffmpeg: String
	})
});

export const subtitle = Record({
	start: Number,
	end: Number,
	text: String
});

export const subtitles = Array(subtitle);

export const VideoData = Record({
	vid_id: String,
	subtitles: subtitles,
	frames_generated: Boolean,
	frames: Number,
	scenes: Array(Number)
});