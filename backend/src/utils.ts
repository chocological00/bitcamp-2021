import { FastifyReply } from 'fastify';

export function make400(response: FastifyReply, reason?: string): void
{
	void response.code(400).send('Could not parse data' + (reason ? ` - ${reason}` : ''));
}