import fastify from 'fastify';
import path from 'path';
import fs from 'fs';
import fastifySession from 'fastify-session';
import fastifyCookie from 'fastify-cookie';
import fastifySocketIo from 'fastify-socket.io';
import fastifyStatic from 'fastify-static';
import dbMan from './db';
import {CONFIG} from './types';


export const config = CONFIG.check(JSON.parse(fs.readFileSync('./data/data.json', 'utf8')));
export const ws_map = new Map<string, string>();
export const db = new dbMan();
export const processing: Set<string> = new Set();

// eslint-disable-next-line @typescript-eslint/require-await
async function main() 
{
	ws_map.set('test', 'e0TuRfC6cUk');


	await db.connect();

	// setup fastify
	const server = fastify({logger: true});
	void server.register(fastifyCookie);
	void server.register(fastifySession, {
		secret: config.storage.session_secret,
		cookie: {
			secure: 'auto'
		}
	});
	//void server.register(fastifyCors, { origin: true });
	
	void server.register(fastifySocketIo, {
		path: '/ws/'//,
		//pingTimeout: 6000,
		//pingInterval: 2000
	});
	
	void server.register(fastifyStatic, 
		{
			root: path.join(__dirname, '../../../../frontend/build/'),
			wildcard: false
		});

	// eslint-disable-next-line @typescript-eslint/no-var-requires
	void server.register(require('./routes/request'));
	// eslint-disable-next-line @typescript-eslint/no-var-requires
	void server.register(require('./routes/ws'));
	
	void server.get('/*', (req, res) => 
	{
		void res.type('text/html').send(fs.createReadStream(path.join(__dirname, '../../../../frontend/build/index.html')));
	});

	server.listen(config.server.port, config.server.addr, (err, address): void => 
	{
		if (err) 
		{
			// eslint-disable-next-line no-console
			console.error(err);
			process.exit(1);
		}
		// eslint-disable-next-line no-console
		console.log(`Server listening at ${address}`);
	});
}

void main();