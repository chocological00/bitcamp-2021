import {Client} from 'pg';
import {config} from './index';
import {VideoData} from './types';
import {Static} from 'runtypes';

const GET_VIDEO = 'SELECT * FROM videos WHERE vid_id=$1;';
const INSERT_VIDEO = 'INSERT INTO videos (vid_id, subtitles, frames_generated, frames, scenes) VALUES ($1, $2, $3, $4, $5);';
const UPDATE_VIDEO = 'UPDATE videos SET vid_id=$1, subtitles=$2, frames_generated=$3, frames=$4, scenes=$5;';

class DBManager 
{
	db: Client;

	constructor()
	{
		this.db = new Client(config.storage.postgres);
	}

	public connect(): Promise<void> 
	{
		return this.db.connect();
	}

	public async getVideo(id: string): Promise<null|Static<typeof VideoData>>
	{
		const res = await this.db.query<Static<typeof VideoData>>(
			{
				text: GET_VIDEO,
				values: [id]
			});
		return res.rowCount === 0 ? null : VideoData.check(res.rows[0]);
	}

	public async insertVideo(video: Static<typeof VideoData>): Promise<boolean>
	{
		const subtitle_str = JSON.stringify(video.subtitles);
		const scenes = JSON.stringify(video.scenes);
		let query;
		if (await this.getVideo(video.vid_id))
		{
			query = UPDATE_VIDEO;
		} 
		else 
		{
			query = INSERT_VIDEO;
		}
		const res = await this.db.query(
			{
				text: query,
				values: [video.vid_id, subtitle_str, video.frames_generated, video.frames, scenes]
			});
		return res.rowCount === 1;
	}
}
export default DBManager;