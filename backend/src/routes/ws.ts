import {FastifyPluginCallback} from 'fastify';
import {ProgressStage, WebsocketProgress, FailMessage} from '../../../common/types';
import * as index from '../index';
import * as process from '../analyze';

const cb: FastifyPluginCallback = (fastify, opts, done) => 
{
	fastify.io.on('connection', (socket) => 
	{
		// eslint-disable-next-line no-console
		console.log('Connecting socket.');
		const query = socket.handshake.query;
		let video_id = '';

		if (query && query.uuid) 
		{
			const map_res = index.ws_map.get(query.uuid);
			if (map_res) 
			{
				video_id = map_res;
			}
			else 
			{
				socket.emit('fail', 'bad_uuid');
				socket.disconnect(true);
				return;
			} 
		}
		else 
		{
			socket.emit('fail', 'bad_uuid');
			socket.disconnect(true);
			return;
		}

		const progressFn = (stage: ProgressStage, progress: number) => 
		{
			if (socket.connected) 
			{
				console.log('CB', stage, progress);
				const toSend: WebsocketProgress = {
					progress,
					stage
				};
				socket.emit('progress', toSend);
			}
			return socket.connected;
		};

		void process.analyze(video_id, progressFn).then((data) =>
		{
			if (socket.connected) 
			{
				// eslint-disable-next-line no-console
				console.log('Done!');
				socket.emit('done', data);
				socket.disconnect();
			}
		})
			.catch((err: FailMessage) =>
			{
				// eslint-disable-next-line no-console
				console.log('Failure: ', err);
				index.processing.delete(video_id);
				if (socket.connected) 
				{
					socket.emit('fail', err);
					socket.disconnect();
				}
			});


		socket.on('disconnect', () => 
		{
			// eslint-disable-next-line no-console
			console.log('Disconnecting socket.');
		});
	});
	done();
};

module.exports = cb;