import {FastifyPluginCallback} from 'fastify';
import {RequestBody, RequestResponse} from '../../../common/types';
import * as commonUtils from '../../../common/utils';
import * as utils from '../utils';
import * as index from '../index';
import {v4 as uuidv4} from 'uuid';

const cb: FastifyPluginCallback = (fastify, opts, done) => 
{
	fastify.post<{
		Body: RequestBody
	}>('/request', (req, res) => 
	{
		const body = req.body;
		if (body && body.url) 
		{
			let id;
			if ((id = commonUtils.validateURL(body.url)) !== null) 
			{
				
				const uuid = uuidv4();
				index.ws_map.set(uuid, id);
				const response: RequestResponse = {
					video_id: id,
					uuid: uuid
				};
				void res.send(response);
			}
			else
			{
				utils.make400(res, 'URL validation');
			}
		}
		else 
		{
			utils.make400(res, 'schema');
		}
	});

	done();
};

module.exports = cb;